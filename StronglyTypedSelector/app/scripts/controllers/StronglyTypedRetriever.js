//adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('StronglyTypedRetriever', 
function($q, $http, umbRequestHelper) {
    //the factory object returned
    return {
        //this calls the ApiController we setup earlier
        GetStronglyTypedItemFor: function (inputValue) {
            return umbRequestHelper.resourcePromise(
                $http.get("/umbraco/api/GvApi/GetStronglyTypedItem/?inputValue="+inputValue),
                "Failed to retrieve all Person data"
            );
        }
    };
}
); 