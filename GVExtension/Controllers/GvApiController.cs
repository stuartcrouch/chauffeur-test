﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Umbraco.Core.Logging;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;
using Umbraco.Core.Models;
using GVExtension.Models;

namespace GVExtension.Controllers
{
    public class GvApiController : UmbracoApiController
    {
        // GET /umbraco/api/GvApi/GetStronglyTypedItem/
        public object GetStronglyTypedItem(string inputValue)
        {
            GVTestObject GVType = new GVTestObject();
            var GVDataTypes = Enum.GetValues(typeof(GoldVisionDataType));
            var random = new Random();

            GVType.GVDataType = (GoldVisionDataType)GVDataTypes.GetValue(random.Next(GVDataTypes.Length));
            GVType.Value = inputValue;

            return Json(GVType);
        }


        // GET /umbraco/api/GvApi/GetNavigation/
        public object GetNavigation()
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            try
            {
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var selection = umbracoHelper.TypedContentAtRoot().First().Children.Where(x => x.IsVisible()).ToArray();
               
                var dictionaryData = new Dictionary<string, object>();
                foreach (var item in selection)
                {
                    dictionaryData[item.Name] = item.Url;
                }

                return Json(dictionaryData);
            }
            catch (Exception ex)
            {
                LogHelper.Info(typeof(GvApiController), String.Format("Error details: {0}", ex.Message));

                //throw 500
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Json(ex), Configuration.Formatters.JsonFormatter);
            }
        }

        public object GetListFor(string inputValue)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            try
            {
                var dictionaryData = new Dictionary<string, object>();

                for (int i = 1; i < 5; i++)
                {
                    dictionaryData[inputValue + "Key" + i.ToString()] = inputValue + "defaultValue" + i.ToString();        
                }
                return Json(dictionaryData);
            }
            catch (Exception ex)
            {
                LogHelper.Info(typeof(GvApiController), String.Format("Error details: {0}", ex.Message));

                //throw 500
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Json(ex), Configuration.Formatters.JsonFormatter);
            }
        }

        // GET: /umbraco/api/GvApi/getData/
        public object GetData(string url)
        {
            HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

            try
            {
                url = HandleUrlDecoding(url);

                var data = UmbracoContext.Current.ContentCache
                    .GetByXPath(string.Format(@"//*[@isDoc and @urlName=""{0}""]", url))
                    .FirstOrDefault();

                if (data != null)
                {
                    var dictionaryData = new Dictionary<string, object>();
                    foreach (var publishedProperty in data.Properties)
                    {
                        dictionaryData[publishedProperty.PropertyTypeAlias] = publishedProperty.Value.ToString();
                    }
                    
                    return Json(dictionaryData);
                }

                return Request.CreateResponse(HttpStatusCode.NoContent);
                
            }
            catch (Exception ex)
            {
                LogHelper.Info(typeof(GvApiController), String.Format("Error details: {0}", ex.Message));

                //throw 500
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Json(ex), Configuration.Formatters.JsonFormatter);
            }
        }

        private void HandleSerializationError(object sender, ErrorEventArgs errorArgs)
        {
            errorArgs.ErrorContext.Handled = true;
        }

        public string HandleUrlDecoding(string url)
        {
            url = HttpUtility.UrlDecode(url);

            var urlName = url == "/"
                ? "home"
                : url.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();


            if (!string.IsNullOrEmpty(urlName))
            {
                urlName = urlName.Replace(".aspx", "");
                urlName = urlName.ToLower();
            }

            return urlName;
        }
    }
}