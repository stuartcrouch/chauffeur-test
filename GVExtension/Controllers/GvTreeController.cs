﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Mvc;
using umbraco.BusinessLogic.Actions;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;

namespace GVExtension.Controllers
{
    [PluginController("GVExtension")]
    [Tree("GVExtension", "GVExtensionTree", "My custom section", iconClosed: "icon-doc")]
    public class GvTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();
            var item = CreateTreeNode("dashboard", id, queryStrings, "My item", "icon-truck", true);
            nodes.Add(item);
            return nodes;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection
            {
                DefaultMenuAlias = ActionNew.Instance.Alias
            };
            menu.Items.Add<ActionNew>("Create");
            return menu;
        }
    }
}