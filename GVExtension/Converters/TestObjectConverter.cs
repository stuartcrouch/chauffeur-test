﻿using GVExtension.Models;
using System;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core;
using Newtonsoft.Json;
using Umbraco.Core.Logging;

namespace GVExtension.Converters
{
    [PropertyValueType(typeof(GVTestObject))]
    [PropertyValueCache(PropertyCacheValue.All, PropertyCacheLevel.Content)]
    public class TestObjectConverter : PropertyValueConverterBase
    {
        public override bool IsConverter(PublishedPropertyType propertyType)
        {
            return propertyType.PropertyEditorAlias.Equals("StronglyTypedSelector");
        }

        public override object ConvertDataToSource(PublishedPropertyType propertyType, object source, bool preview)
        {
            if (source == null) return null;

            var sourceString = source.ToString();
            if (sourceString.DetectIsJson())
            {
                try
                {
                    var obj = JsonConvert.DeserializeObject<GVTestObject>(sourceString);
                    return obj;
                }
                catch (Exception ex)
                {
                    LogHelper.Info(typeof(TestObjectConverter), String.Format("Error details: {0}", ex.Message));
                    return null;
                }
            }
            return sourceString;

        }
    }

}