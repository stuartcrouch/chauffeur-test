﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GVExtension.Models
{
    public enum GoldVisionDataType
    {
        GVString,
        GVList,
        GVInt,
        GVCurrency
    }

    public class GVTestObject
    {
        [UIHint("GVDataType")]
        public GoldVisionDataType GVDataType { get; set; }
        public string Value { get; set; }
    }
}