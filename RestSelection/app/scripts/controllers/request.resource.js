//adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('requestResource', 
function($q, $http, umbRequestHelper) {
    //the factory object returned
    return {
        //this calls the ApiController we setup earlier
        GetListFor: function (inputValue) {
            return umbRequestHelper.resourcePromise(
                $http.get("/umbraco/api/GvApi/GetListFor/?inputValue="+inputValue),
                "Failed to retrieve all Person data"
            );
        }
    };
}
); 