angular.module('umbraco').controller('RestSelectionController', function($scope, $routeParams, requestResource) {
  console.log('Hello from RestSelectionController v2');
  
  requestResource.GetListFor($routeParams.inputValue).then(function(response){
        $scope.data = response;
    });

    $scope.setSelectedItem = function(item) {
        $scope.model.value = item;
    };

    $scope.isSelectedItem = function(item) {
        return item === $scope.model.value ? "selected" : "";
    };
});

