# RestSelection

Property editor that queries an API and shows the results in a dropdown list

## Setup

### Install Dependencies

```bash
npm install -g grunt-cli
npm install
```

### Build

```bash
grunt
```

### Watch

```bash
grunt watch
```

## Example 
# grunt --target=C:\Users\stuar\Documents\Projects\CoffeeLovers\CoffeeLovers