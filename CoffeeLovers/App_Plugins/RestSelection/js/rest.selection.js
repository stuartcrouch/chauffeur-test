//adds the resource to umbraco.resources module:
angular.module('umbraco.resources').factory('requestResource', 
function($q, $http, umbRequestHelper) {
    //the factory object returned
    return {
        //this calls the ApiController we setup earlier
        GetListFor: function (inputValue) {
            return umbRequestHelper.resourcePromise(
                $http.get("/umbraco/api/GvApi/GetListFor/?inputValue="+inputValue),
                "Failed to retrieve all Person data"
            );
        }
    };
}
); 
angular.module('umbraco').controller('RestSelectionController', function($scope, $routeParams, requestResource) {
  console.log('Hello from RestSelectionController v2');
  
  requestResource.GetListFor($routeParams.inputValue).then(function(response){
        $scope.data = response;
    });

    $scope.setSelectedItem = function(item) {
        $scope.model.value = item;
    };

    $scope.isSelectedItem = function(item) {
        return item === $scope.model.value ? "selected" : "";
    };
});

