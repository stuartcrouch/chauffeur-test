angular.module("umbraco")
.controller("My.MarkdownEditorController",
//inject umbracos assetsService
function ($scope,assetsService) {

    //tell the assetsService to load the markdown.editor libs from the markdown editors
    //plugin folder
    assetsService
        .load([
            "~/App_Plugins/MarkDownEditor/lib/markdown.converter.js",
            "~/App_Plugins/MarkDownEditor/lib/markdown.sanitizer.js",
            "~/App_Plugins/MarkDownEditor/lib/markdown.editor.js",
            "~/App_Plugins/MarkDownEditor/lib/less/less-1.2.2.min.js"
        ])
        .then(function () {
            var mdConvertor = new Markdown.Converter();
            var mdEditor = new Markdown.Editor(mdConvertor, "-" + $scope.model.alias);
            mdEditor.run();
        });

    //load the separate css for the editor to avoid it blocking our js loading
    //assetsService.loadCss("~/App_Plugins/MarkDownEditor/lib/markdown.css");
});