import 'rxjs/add/operator/switchMap';
import { Component, OnInit, HostBinding } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap, NavigationStart } from '@angular/router';
import { UmbracoService } from "./umbraco.service";
import { slideInDownAnimation } from "../_animations/animations"

@Component({
  templateUrl: './umbraco-display.component.html',
  providers: [Location],
  //styleUrls: ['./umbraco-display.component.css']
  animations: [slideInDownAnimation],
  host: { '[@routeAnimation]': '' }
})

export class UmbracoDisplayComponent implements OnInit {
  //@HostBinding('@routeAnimation') routeAnimation = true;
  // @HostBinding('style.display')   display = 'block';
  // @HostBinding('style.position')  position = 'absolute';

  umbResponse: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: UmbracoService,
    private location: Location,
  ) { }

  ngOnInit() {

    // Load the data for the page we started on
    this.umbResponse = this.service.getAll(location.pathname).subscribe(data => {this.umbResponse = data});

    // Watch for any changes in the URL and update the data 
    this.router.events.subscribe(
      event => {
        if (event instanceof NavigationStart) {
          this.service.getAll(event.url).subscribe(data => {this.umbResponse = data});
        }
    });
    
  }
}
