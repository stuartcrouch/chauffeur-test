import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'
import { Observable } from 'rxjs/Observable';
import { Configuration } from "../app.configuration";
import { Injectable } from '@angular/core';

@Injectable()
export class UmbracoService {
  private actionUrl: string;

  constructor(private _http: Http , private _configuration: Configuration) {
    this.actionUrl = _configuration.apiRoot;
  }

  public getAll = (inUrl: string): Observable<any> => {
    return this._http.get(this.actionUrl + "getData/?url=" + inUrl)
      .map((res: Response) => res.json());
  }
}


