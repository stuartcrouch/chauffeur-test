import { NgModule } from '@angular/core';
import { UmbracoDisplayComponent } from './umbraco-display.component';
import { UmbracoService } from './umbraco.service'
import { Configuration } from '../app.configuration'

@NgModule({
  declarations: [
    UmbracoDisplayComponent
  ],
  providers: [
    UmbracoService,
    Configuration
   ]
})
export class UmbracoModule {}
    
