import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UmbracoDisplayComponent } from "./umbraco/umbraco-display.component";

const routes: Routes = [
  { path: 'about', component: UmbracoDisplayComponent, data: { state: 'about' }},
  { path: 'locations', component: UmbracoDisplayComponent, data: { state: 'locations' }},
  { path: 'contact', component: UmbracoDisplayComponent, data: { state: 'contact' }},
  { path: '**', component: UmbracoDisplayComponent, data: { state: '*' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
