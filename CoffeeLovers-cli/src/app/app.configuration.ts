import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
  apiRoot: string;

  constructor() {
    this.apiRoot = '/umbraco/api/GvApi/';
  }
}
