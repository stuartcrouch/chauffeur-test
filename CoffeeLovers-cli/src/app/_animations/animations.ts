// import the required animation functions from the angular animations module
import { trigger, state, animate, transition, style, query as q, group } from '@angular/animations';
import { AnimationEntryMetadata } from "@angular/core";
 
// this stops angular throwing up errors about being able to process the query
export function query(s, a) {
    return q(s, a, {optional: true});
}

export const slideInDownAnimation: AnimationEntryMetadata =
trigger('routeAnimation', [
    transition('* <=> *', [    
        query(':enter, :leave', style({ position: 'fixed', width:'100%' })),
        group([ 
          query(':enter', [
            style({ transform: 'translateX(100%)' }),
            animate('0.5s ease-in-out', style({ transform: 'translateX(0%)' }))
          ]),
          query(':leave', [
            style({ transform: 'translateX(0%)' }),
            animate('0.5s ease-in-out', style({ transform: 'translateX(-100%)' }))
          ]),
        ])
    ])
]);