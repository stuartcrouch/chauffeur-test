import { Component } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/core';
import { slideInDownAnimation } from "./_animations/animations"
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ slideInDownAnimation ]
})
export class AppComponent {
  data: any = null;

  getState(outlet) {
    const animation = outlet.activatedRouteData.state || {};
    return animation;
  }
}
