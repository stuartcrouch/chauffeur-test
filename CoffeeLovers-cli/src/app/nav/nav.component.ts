import { Component, OnInit } from '@angular/core';
import { NavService } from './nav.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css'],
  providers: [NavService]
})
export class NavComponent implements OnInit {
  selection: any;
  arrayOfKeys: any;

  constructor(
    private service: NavService
  ) { }

  ngOnInit() {    
    this.service.get().subscribe(data => { 
      this.arrayOfKeys = Object.keys(data);
      this.selection = data;
    });
    
  }

}
